<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="../taglib.jsp" %>
<%@ include file="../include_recharge.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport"
          content="width=device-width,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="format-detection" content="telephone=no"/>
    <title>公众号充值首页</title>
</head>

<body>
<input id="agentCode" type="hidden" value="${agentCode}">
<div id="phone" class="top">
    <input id="phoneCode" type="tel" placeholder="请输入手机号码" class="input_style" maxlength="11"/>
    <c:if test="${empty userHeadImg}">
        <img id="iconUser" src="${__PAGEROOT__}/img/username.png" class="useravatar"/>
    </c:if>
    <c:if test="${not empty userHeadImg}">
        <img id="iconUser" src="${userHeadImg}" class="useravatar"/>
    </c:if>
    <%--<div style="display: block; color: #c8c8c8; font-size: 28px; padding-left: 10px;">默认号码</div>--%>
</div>
<h3>充话费</h3>
<!--充值选项部分-->
<div id="connent" class="recharge">

</div>

<div id="valuedes" class="amount">请选择充值金额</div>
<input type="hidden" id="amountvalue" value=""/>
<div class="button">
    <button class='distance' id="buttonPay">立即支付</button>
</div>
<div class="button">
    <!--邀请码输入框-->
    <span>请输入邀请码</span><input type="text" class='input_style' id="inviteCode"/>
</div>
<!--增加填写用户来源的输入框-->
<c:if test="${agentCode=='110102'}">
    <div class='explain'>
        <h4>充值时间：</h4>
        <div style="margin-left: 25px;">
            <p>话费快充7x24小时自动充值，无任何时间限制（运营商系统维护除外）。</p>
        </div>
        <h4>到账时间：</h4>
        <div style="margin-left: 25px;">
            <p>充值后，1-10分钟即可到账，遇到月初月末高峰期会有些许延迟，详细到账时间以当地运营商查询为准，到账后会后运营商发来的到账短信（如遇运营商系统忙或手机已欠费停机会没有短信通知）。</p>
        </div>
        <h4>充值号码：</h4>
        <div style="margin-left: 25px;">
            <p>在网上提交的充值内容，是具备法律效应的交易指令。<span
                    style="color:red">所以请您务必在充值时认真仔细的填写所要充值的手机号码，</span>如因错填号码而导致的损失均有买家负责（<span style="color:red">为保障您的合法利益，请慎重填写充值号码</span>）。
            </p>
        </div>
        <h4>温馨提示：</h4>
        <div style="margin-left: 25px;">
            <p>购买前请务必确认充值前的欠费金额，如充值金额不足以缴纳欠费，可能导致您充值后仍处于欠费停机状态，造成不必要误解。</p>
            <p>购买时，<span style="color:red">请准确填写需要充值的手机号码</span>，因您提供的充值号码导致充值损失，本店不承担责任。<span style="color:red">一旦充值成功，本店不提供退换服务</span>。
            </p>
            <p>每月月初3日内以及月底最后3日内为缴费高峰期，可能会出现运营商系统繁忙，敬请谅解。</p>
            <p>充值成功后，可能没有短信说明，或短信提示内容不准确，请您拨打运营商客服电话（移动10086、联通10010、电信10000）查询到账情况。</p>
            <p>本店不参加当地运营商的任何优惠活动。</p>
            <p>本店不提供充值发票，建议联系运营商索要电子发票。</p>
        </div>
    </div>
</c:if>
<c:if test="${agentCode=='110103'}">
    <div class='explain'>
        <h4>话费慢充温馨说明：</h4>
        <div style="margin-left: 25px;">
            <p>自动到账。</p>
            <p>可以抵扣月租、套餐费、短信、流量、语音通讯费等。</p>
            <p>输错号码请立即联系客服，15点前可以修改。</p>
        </div>
        <h4>关于充不了：</h4>
        <div style="margin-left: 25px;">
            <p>只限广东、南京、淮安移动号码，其他地区充不了。</p>
            <p>停机、欠费、副卡充不了。</p>
            <p>其他非实名制、运营商黑名单、做过某些特殊套餐绑定的充不了。</p>
            <p>一个号码一个月只能买一次，包括在别的地方购买过慢充。</p>
            <p>发现充不了，客服会第一时间联系。</p>
            <p>如果已经欠费了，请移步★话费快充★，也有小小优惠哈。</p>
        </div>
        <h4>关于到账时间：</h4>
        <div style="margin-left: 25px;">
            <p>15点前付款，最快可在当天24点到账。</p>
            <p>15点后付款，最快可在次日24点到账。</p>
            <p>每周五15点后付款，最快可在下周一24点到账。</p>
            <p>每月最后一天、月初第一天的顺延到月初第二天24点到账。</p>
            <p>节假日跟周末一样顺延。</p>
        </div>
        <h4>关于发票：</h4>
        <div style="margin-left: 25px;">
            <p>慢充特惠话费，不支持开发票，介意者勿拍！</p>
        </div>
        <h4>关于短信：</h4>
        <div style="margin-left: 25px;">
            <p>话费到账会收到运营商发送的短信通知，如遇系统繁忙也有可能收不到短信或者是短信延迟。</p>
        </div>
    </div>
</c:if>
</body>
<script type="text/javascript">
  $(function () {

    var load;//加载框
    var passFlag;//是否可以充值的标记，true为可以，默认是false

    initData();
    addActions();


    //初始化数据
    function initData() {
      //初始化加载框
      load = new Loading();
      load.init();

      var userSubscribeFlag = '${subscriberFlag}';

      if (userSubscribeFlag == '0') {
        alert("您未关注公众号，不可进行充值，请先关注‘帮你充话费’公众号");
        window.location.href = "https://mp.weixin.qq.com/mp/profile_ext?action=home&__biz=MzI5OTEyMzY5NA==&scene=126#wechat_redirect";
        return;
      }

      passFlag = false;
      //初始化微信JSSDK
      wx.config({
        debug: false,
        appId: '${appId}',
        timestamp: '${timestamp}',
        nonceStr: '${nonceStr}',
        signature: '${signature}',
        jsApiList: ['checkJsApi', 'chooseWXPay']
      });

      //微信JSSDK初始化回调
      wx.ready(function () {
        wx.checkJsApi({
          jsApiList: ['scanQRCode'],
          success: function (res) {
            console.log("微信jssdk初始化成功");
          }
        });
      });

      //TODO INVITE 如果用户已经被邀请，则不显示inviteCode控件
      var isInvite = '${isInvite}';
      if(isInvite == true){
        $("#inviteCode").hide();
      }

      //TODO INVITE 如果请求参数中有邀请码参数，则需要默认添加邀请码到inviteCode控件中
      var userInviteCode = '${userInviteCode}';
      if(userInviteCode != null ){
         $("#inviteCode").text(userInviteCode);
      }
    }


    //添加事件
    function addActions() {
      //用户头像按钮
      $("#iconUser").click(function () {
        //window.location.href = '${__URL__}/MpRechargeCtrl/rechargeQueryResult'
        window.location.href = '${__URL__}/MpRechargeCtrl/userCenter'
      });

      //号码输入框失去焦点事件
      $("#phoneCode").blur(function () {
        passFlag = false;//重新输入了号码，状态改为false
        var phoneCode = $("#phoneCode").val();
        var agentCode = $("#agentCode").val();
        if (phoneCode.length == 11) {
          //调用后台获取充值选项
          load.start();//显示加载框
          var postData = new Object();
          postData.phoneCode = phoneCode;
          postData.agentCode = agentCode;
          $.ajax({
            type: "POST",
            url: "${__URL__}/MpRechargeCtrl/queryRechargeItemByPhoneCode",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(postData),
            dataType: "json",
            success: function (data) {
              load.stop();
              console.log("调用查询号码选项单接口返回:" + JSON.stringify(data));
              if (data.retCode == '0000') {
                //调用成功并且有数据，回填页面元素
                fillChargeItemArea(data.reChargeItemList);
              } else {
                var retMsg = data.retMsg;
                //alert("调用查询号码选项单接口出现错误:" + retMsg);
                fillChargeItemArea(null);
              }
            },
            error: function (data) {
              load.stop();
              alert("调用查询号码选项单接口返回，失败参数:" + JSON.stringify(data));
            }
          });
        }
      });


      //充值按钮事件
      $("#buttonPay").on('click', (function () {
        //第一个bg的元素
        var choosedItem = $(".gold li.bg:first");
        var amount = choosedItem.data("price");
        var offerId = choosedItem.data("id");
        var phoneCode = $("#phoneCode").val();
        var agentCode = $("#agentCode").val();
        var inviteCode = $("#inviteCode").val();//邀请码


        //输入的参数有效性检测
        if (!phoneCode) {
          alert("别忘了输入号码");
          return;
        }
        if (!isPoneAvailable(phoneCode)) {
          alert("请输入正确的手机号");
          return;
        }

        if (!passFlag) {
          alert("请选择充值金额");
          return;
        }

        load.start();//显示加载框
        //请求参数
        var postData = new Object();
        postData.amount = amount;
        postData.offerId = offerId;
        postData.phoneCode = phoneCode;
        postData.agentCode = agentCode;
        postData.inviteCode = inviteCode;
        $.ajax({
          type: "POST",
          url: "${__URL__}/MpRechargeCtrl/weixinPayOrderWithRechargeItem",
          contentType: "application/json; charset=utf-8",
          data: JSON.stringify(postData),
          dataType: "json",
          success: function (data) {
            load.stop();
            console.log("调用微信统一下单接口返回:" + JSON.stringify(data));
            if (data.retCode == '0000') {
              //调用成功
              var prepayId = data.weixinOrderNo;
              console.log("初始化微信支付");
              wx.chooseWXPay({
                timestamp: data.timeStamp, // 支付签名时间戳，注意微信jssdk中的所有使用timestamp字段均为小写。但最新版的支付后台生成签名使用的timeStamp字段名需大写其中的S字符
                nonceStr: data.nonceStr, // 支付签名随机串，不长于 32 位
                package: "prepay_id=" + prepayId, // 统一支付接口返回的prepay_id参数值，提交格式如：prepay_id=***）
                signType: data.signType, // 签名方式，默认为'SHA1'，使用新版支付需传入'MD5'
                paySign: data.paySign, // 支付签名
                success: function (res) {
                  //支付调用成功
                  console.log("微信支付jssdk调用返回:" + JSON.stringify(res));
                  alert("支付成功，我们正在拼命提交到运营商，充值金额48小时内到账");
                  window.location.href = '${__URL__}/MpRechargeCtrl/rechargeQueryResult';
                }
              });
            } else {
              var retMsg = data.retMsg;
              alert("调用微信统一下单接口出现错误:" + retMsg);
            }
          },
          error: function (data) {
            load.stop();
            alert("调用微信统一下单接口失败，失败参数:" + JSON.stringify(data));
          }
        });
      }));
    }

    //去掉所有属性方法
    function removeDivClass() {
      $.each($(".gold li.bg"), function () {
        $(this).removeClass("bg");
      });
    }

    /**
     * 校验手机号
     * @param phoneNo
     * @returns {boolean}
     */
    function isPoneAvailable(phoneNo) {
      var myreg = /^[1][3,4,5,7,8,9][0-9]{9}$/;
      if (!myreg.test(phoneNo)) {
        return false;
      } else {
        return true;
      }
    }

    /**
     * 填充话费充值选项列表
     * @param ChargeItemList
     */
    function fillChargeItemArea(rechargeItemList) {
      $("#connent").empty();
      if (rechargeItemList) {
        //如果有选项，则填充内容
        var itemUl = $("<ul></ul>");
        itemUl.addClass("gold");
        //itemUl.addClass("w");
        itemUl.attr('id', 'recharge_item_ul');
        for (var i = 0; i < rechargeItemList.length; i++) {
          var rechargeItem = rechargeItemList[i];
          //li元素初始化
          var itemLi = $("<li></li>");
          itemLi.val(rechargeItem.parValue);
          itemLi.data("id", rechargeItem.itemId);
          itemLi.data("parvalue", rechargeItem.parValue);
          itemLi.data("price", rechargeItem.itemPrice);

          //充值面额
          var itemH2 = $("<h2></h2>");
          itemH2.html(rechargeItem.itemName);
          itemH2.appendTo(itemLi);

          //充值描述
          var selDiv = $("<div></div>");
          selDiv.addClass("sel");
          selDiv.html(rechargeItem.itemDesc);
          selDiv.appendTo(itemLi);

          //把li添加到ul里面
          itemLi.appendTo(itemUl);
        }
        itemUl.appendTo($("#connent"));
        //绑定充值选项事件
        $(".gold li").click(function () {
          removeDivClass();
          $(this).addClass("bg");
          var value = $(this).attr("value");
          $("#valuedes").html("<p>所选充值金额：" + value + "</p>");
          $("#cash").val(value);
          passFlag = true;//选中金额后，可以充值了
        });
      } else {
        $("#connent").html("对不起，暂时不支持您所在的地区，我们正在加速接入");
      }
    }
  });
</script>
</html>